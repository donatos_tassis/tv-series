import React, { memo } from 'react';

import { connect } from 'react-redux';
import {useHistory, useRouteMatch} from 'react-router';
import logo from '../../assets/images/tv.png';
import styles from './Logo.module.css';
import * as action from "../../store/actions";

const Logo = memo(({resetSeries}) => {
  const history = useHistory();
  const home = useRouteMatch({path: '/', exact: true});

  const redirectHome = () => {
    resetSeries();
    history.push('/');
  }

  return (
    <div className={styles.Logo}>
      <img
        src={logo}
        alt="TV-Tracker"
        onClick={() => { home ? history.go() : redirectHome()}}/>
    </div>
  );
});

const mapDispatchToProps = dispatch => {
  return {
    resetSeries: () => dispatch(action.fetchPopularSeriesReset()),
  }
}

export default connect(null, mapDispatchToProps)(Logo);