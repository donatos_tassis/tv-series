import React, { memo } from 'react';

import Table from '@material-ui/core/Table';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import TableBody from '@material-ui/core/TableBody';
import Paper from '@material-ui/core/Paper';
import EpisodeTableRow from './EpisodeTableRow/EpisodeTableRow';
import { withStyles } from '@material-ui/core/styles';

import classes from './styles';
import styles from './EpisodesTable.module.css';

const EpisodesTable = memo((props) => {

  const {classes, columns, rows} = props;

  return (
    <div className={styles.EpisodesTable}>
      <Paper className={classes.paper}>
        <TableContainer className={classes.container}>
          <Table stickyHeader className={classes.root}>
            <TableHead className={classes.head}>
              <TableRow className={classes.row}>
                {columns.map(column => {
                  return (
                    <TableCell style={{textAlign: 'center'}} className={classes.head} key={column.id}>
                      <div className={styles.CellContent}>{column.label}</div>
                    </TableCell>
                  )
                })}
              </TableRow>
            </TableHead>
            <TableBody className={classes.body}>
              {rows.map(row => {
                return <EpisodeTableRow key={row.episode_number} row={row} columns={columns}/>
              })}
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
    </div>
  );
});

export default withStyles(classes)(EpisodesTable);