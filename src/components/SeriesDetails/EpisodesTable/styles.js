import theme from '../../../helper/breakpoints';

const styles = () => ({
  paper : {
    [theme.breakpoints.down('xs')] : {
      boxShadow: 'none'
    }
  },
  root: {
    [theme.breakpoints.down('xs')] : {
      display: 'block',
      padding: '0 10px'
    }
  },
  container: {
    maxHeight: 450,
    overflowY: 'scroll',
    maxWidth: '100%',
    overflowX: 'hidden',
    scrollbarWidth: 'thin',
    scrollbarColor: 'rgba(62,14,139,0.3)',
    '&::-webkit-scrollbar': {
      width: '5px'
    },
    '&::-webkit-scrollbar-thumb': {
      backgroundColor: 'rgba(62,14,139,0.3)'
    },
    [theme.breakpoints.down('xs')] : {
      display: 'block',
      maxHeight: 'none',
      overflowY: 'hidden'
    }
  },
  head: {
    backgroundColor: '#3e0e8b',
    color: '#fff',
    [theme.breakpoints.down('xs')] : {
      display: 'block',
      position: 'absolute',
      top: '-9999px',
      left: '-9999px'
    },
  },
  row: {
    [theme.breakpoints.up('sm')] : {
      boxShadow: '1px 1px 7px silver',
    },
    [theme.breakpoints.down('xs')] : {
      display: 'block',
      borderRadius: '10px',
      border: 'solid 1px silver',
      boxShadow: '1px 1px 7px silver',
      margin: '0 0 1rem 0',
      '&:nth-child(odd)': {
        background: 'rgba(62,14,139,0.15)'
      },
      '&:nth-child(even)': {
        background: 'rgba(62,14,139,0.3)'
      }
    }
  },
  body: {
    [theme.breakpoints.down('xs')] : {
      display: 'block'
    }
  },
  cell: {
    [theme.breakpoints.up('sm')] : {
      paddingRight: '0',
      paddingLeft: '0',
      borderBottom: '1px solid lightgrey',
      '&:nth-child(even)': {
        background: 'rgba(62,14,139,0.15)'
      },
      '&:nth-child(odd)': {
        background: 'rgba(62,14,139,0.3)'
      }
    },
    [theme.breakpoints.down('xs')] : {
      minHeight: '20px',
      paddingRight: '0',
      display: 'block',
      borderBottom: '1px solid #eee',
      position: 'relative',
      paddingLeft: '40%',
      '&:before' : {
        position: 'absolute',
        fontWeight: 'bold',
        left: '6px',
        width: '55%',
        margin: 'auto 0',
        paddingRight: '10px',
        whiteSpace: 'nowrap'
      }
    },
  }
});

export default styles;