import React, { useState } from 'react';

import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import WatchedCheckBox from '../../../UI/WatchedCheckBox/WatchedCheckBox';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import IconButton from '@material-ui/core/IconButton';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import { withStyles } from '@material-ui/core/styles';
import theme from '../../../../helper/breakpoints';

import classes from '../styles';
import styles from './EpisodeTableRow.module.css';

const EpisodeTableRow = (props) => {
  const { classes, row, columns } = props;

  const match = useMediaQuery(theme.breakpoints.down('xs'));
  const [open, setOpen] = useState(false);

  const expandableCells = row => {
    return columns.filter((column, index) => index !== 0)
      .map(column => {
        const id = row.episode_number;
        const value = (row[column.id] !== null) ? row[column.id] : '';
        return (
          <TableCell key={column.id} data-label={column.label} className={classes.cell}>
            <div className={styles.CellContent}>
              { column.id !== 'watched' ? value :
                <WatchedCheckBox id={id} value={value} />
              }
            </div>
          </TableCell>
        )
      })
  }

  return (
    <TableRow key={row.episode_number} className={classes.row}>
      <TableCell data-label={columns[0].label} className={classes.cell}>
        <div className={styles.CellContent}>
          {row[columns[0].id]}
          {match && <IconButton className={styles.CollapseButton} size="small" onClick={() => setOpen(!open)}>
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>}
        </div>
      </TableCell>
      {!match ? expandableCells(row) : (open && expandableCells(row))}
    </TableRow>
  );
};

export default withStyles(classes)(EpisodeTableRow);