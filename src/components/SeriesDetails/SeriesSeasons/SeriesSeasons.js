import React, { memo } from 'react';

import { connect } from 'react-redux';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import { makeStyles } from '@material-ui/core/styles';
import styles from './SeriesSeasons.module.css';
import * as action from "../../../store/actions";

const SeriesSeasons = memo((props) => {

  const seasons = props.seasons.map((season, index) => {
    return <Tab
      key={season.season_number}
      label={season.name}
      index={index}
      id={season.id}
      style={{minWidth : '100px'}}/>
  });

  const useStyles = makeStyles(theme => ({
    root: {
      backgroundColor: '#3e0e8b',
      color: '#fff',
      margin: '2px 0',
      height: '100%',
      borderRadius: '10px',
      border: 'solid 1px silver',
      boxShadow: '1px 1px 7px silver'
    },
    indicator: {
      backgroundColor: '#fff',
    }
  }));

  const classes = useStyles();

  const changeSeason = (event, index) => {
    props.setSeason(index);
    props.seasonChange();
  }

  return (
    <div className={styles.SeriesSeasons}>
      <AppBar className={classes.root} position="static">
        <Tabs
          classes={{indicator: classes.indicator}}
          value={props.season}
          onChange={changeSeason}
          variant="scrollable"
          scrollButtons="auto"
          aria-label="seasons"
        >
          {seasons}
        </Tabs>
      </AppBar>
    </div>
  );
});

const mapDispatchToProps = dispatch => {
  return {
    seasonChange: () => dispatch(action.seasonChange()),
  }
}

export default connect(null, mapDispatchToProps)(SeriesSeasons);