import React, { useState } from 'react';

import crypto from 'crypto-js';
import Iframe from 'react-iframe';

const Login = () => {

  const randomString = (length) => {
    return [...Array(length)].map(() => Math.random().toString(36)[2]).join('')
  }

  const base64Url = (string) => {
    return string.toString(crypto.enc.Base64)
      .replace(/\+/g, '-')
      .replace(/\//g, '_')
      .replace(/=/g, '');
  }

  const [ state ] = useState(randomString(40));
  const [ verifier ] = useState(randomString(128));
  const [ challenge ] = useState(base64Url(crypto.SHA256(verifier)));

  const loginUrl = new URL(process.env.REACT_APP_API_AUTHORIZE_URL);
  loginUrl.searchParams.append('client_id', 1);
  loginUrl.searchParams.append('redirect_uri', process.env.REACT_APP_AUTH_URL);
  loginUrl.searchParams.append('response_type', 'code');
  loginUrl.searchParams.append('scope', '');
  loginUrl.searchParams.append('state', state);
  loginUrl.searchParams.append('code_challenge', challenge);
  loginUrl.searchParams.append('code_challenge_method', 'S256');

  return (
      <Iframe
        id='loginFrame'
        name='loginFrame'
        url={loginUrl}
        height='100%'
        width='100%'
      />
  );
};

export default Login;