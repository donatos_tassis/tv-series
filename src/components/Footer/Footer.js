import React, { memo } from 'react';

import styles from './Footer.module.css';
import HideComponent from '../../hoc/HideComponent/HideComponent';

const Footer = memo(props => {

  return (
    <HideComponent {...props} direction='up'>
      <footer className={styles.Footer}>
        <p>Footer</p>
      </footer>
    </HideComponent>
  );
});

export default Footer;