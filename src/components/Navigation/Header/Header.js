import React, { memo } from 'react';

import NavigationItems from '../NavigationItems/NavigationItems';
import Logo from '../../Logo/Logo';
import HideComponent from '../../../hoc/HideComponent/HideComponent';

import styles from './Header.module.css';

const Header = memo(props => {

  return (
    <HideComponent {...props} direction='down' reverse={true}>
      <header className={styles.Header}>
        <div className={styles.Logo}>
          <Logo />
        </div>
        <nav>
          <NavigationItems toggleFilters={props.toggleFilters}/>
        </nav>
      </header>
    </HideComponent>
  );
});

export default Header;