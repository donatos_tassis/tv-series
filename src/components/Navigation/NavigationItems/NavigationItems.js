import React, { memo, useState } from 'react';
import { useRouteMatch } from 'react-router';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import BackToHome from '../../UI/BackToHome/BackToHome';
import AuthDialog from '../../AuthDialog/AuthDialog';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

import styles from './NavigationItems.module.css';

const NavigationItems = memo(props => {
  const home = useRouteMatch({path: '/', exact: true});
  const seriesDetails = useRouteMatch('/series/:id');
  const [openAuth, setOpenAuth] = useState(false);

  const openAuthDialog = () => {
    setOpenAuth(true);
  };

  const closeAuthDialog = () => {
    setOpenAuth(false);
  };

  return (
    <React.Fragment>
      <AuthDialog {...props} open={openAuth} close={closeAuthDialog}/>
      <ul className={styles.NavigationItems}>
        <li><b onClick={openAuthDialog}>Login</b></li>
        <li>{home && <FontAwesomeIcon icon={faSearch} onClick={props.toggleFilters}/>}</li>
        <li>{seriesDetails && <BackToHome id={seriesDetails.params.id}/>}</li>
      </ul>
    </React.Fragment>
  );
});

export default NavigationItems;