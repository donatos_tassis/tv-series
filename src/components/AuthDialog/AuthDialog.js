import React, {useEffect, useState} from 'react';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import Login from '../Login/Login';
import Auth from '../Login/Auth';

const AuthDialog = (props) => {

  const [submitted, setSubmitted] = useState(false);

  window.addEventListener('message', (event) => {
    if (event.origin !== process.env.REACT_APP_TV_WEB_BASE_URL)
      return;

    if (event.data.submitted)
      setSubmitted(true);
    console.log('Client received message : ', event.data);
  })

  return (
    <Dialog open={props.open} onClose={props.close} maxWidth='sm' fullWidth>
      <DialogContent style={{
        height: '700px',
        maxHeight: '70vh',
        padding: '0',
        overflow: 'hidden',
        position: 'relative'
      }}>
        {!submitted ? <Login /> : <Auth />}
      </DialogContent>
    </Dialog>
  );
};

export default AuthDialog;