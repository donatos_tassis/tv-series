import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';

import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import styles from './FilterSelectionDialog.module.css';

const FilterSelectionDialog = props => {

  const [ selectedFilter, setSelectedFilter ] = useState('');
  const [ description, setDescription ] = useState('Please choose a filter to proceed');

  const { availableFilters } = props;
  useEffect(() => {
    if(selectedFilter) {
      setDescription(availableFilters[selectedFilter].description);
    }
  }, [setDescription, selectedFilter, availableFilters])

  const changeHandler = event => {
    setSelectedFilter(event.target.value);
  }

  const filters = [];
  for (let key in props.availableFilters) {
    filters.push({id: key, config: props.availableFilters[key]})
  }

  return (
    <div className={styles.FiltersDialog}>
      <Dialog open={props.isOpen} onClose={props.close}>
        <DialogTitle><b>Available Filters</b></DialogTitle>
        <DialogContent>
          <FormControl>
            <InputLabel id="filter-selector-label">Filter</InputLabel>
            <Select
              labelId="filter-selector-label"
              id="filter-selector-label"
              value={selectedFilter}
              onChange={changeHandler}
            >
              {filters.map(filter => {
                return (<MenuItem
                  key={filter.id}
                  value={filter.id}>{filter.config.displayName}</MenuItem>)
              })}
            </Select>
            <FormHelperText>{description}</FormHelperText>
          </FormControl>
        </DialogContent>
        <DialogActions>
          <Button onClick={props.close} color="secondary">
            Cancel
          </Button>
          <Button disabled={!selectedFilter} onClick={() => props.next(selectedFilter)} color="primary">
            Next
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    availableFilters: state.filters.availableFilters,
  }
}

export default connect(mapStateToProps)(FilterSelectionDialog);