import React from 'react';
import { connect } from 'react-redux';
import { useForm } from 'react-hook-form'

import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import FormElement from '../../UI/FormElement/FormElement';

import styles from './FilterDialog.module.css';
import * as action from "../../../store/actions";

const FilterDialog = (props) => {

  let defaultValues = {}
  for (let key in props.availableFilters) {
    defaultValues[key] = props.availableFilters[key].properties.defaultValue
  }

  const { register, handleSubmit, errors, watch, setValue, reset } = useForm({defaultValues});

  const submitFilterHandler = data => {
    let filter = {};
    for(let key in data) {
      let displayValue = data[key];
      if (props.availableFilters[key].type === 'select') {
        const options = props.availableFilters[key].properties.options;
        options.forEach(option => {
          if (option.value.toString() === data[key]) {
            displayValue = option.displayValue;
          }
        })
      }
      if (props.availableFilters[key].type === 'multi-select') {
        const displayValues = [];
        const options = props.availableFilters[key].properties.options;
        options.forEach(option => {
          data[key].forEach(genreId => {
            if (option.value === genreId) {
              displayValues.push(option.displayValue);
            }
          })
        })
        displayValue = displayValues.join(', ')
      }
      if (props.availableFilters[key].type === 'range-slider') {
        displayValue = ' >' + data[key];
      }
      filter = {
        id: key,
        value: data[key],
        title: props.availableFilters[key].displayName,
        displayValue: displayValue
      };
    }

    props.onAddFilter(filter);
    props.close();
    reset({...defaultValues}); // reset after manipulate the data -> add selected filter to the list filters
  }

  const cancelHandler = () => {
    props.close();
    reset({...defaultValues});
  }

  const backHandler = () => {
    props.back();
    reset({...defaultValues});
  }

  let filter = null
  if(props.selectedFilter)
    filter = props.availableFilters[props.selectedFilter];

  return (
    <div className={styles.FilterDialog}>
      <Dialog open={props.isOpen} onClose={props.close} disableEnforceFocus>
        <form onSubmit={handleSubmit(submitFilterHandler)}>
          <DialogTitle><b>{filter && filter.description}</b></DialogTitle>
          <DialogContent>
            {filter && (
              <React.Fragment>
                <FormElement
                  validation={filter.validation}
                  register={register}
                  watch={watch}
                  setValue={setValue}
                  name={props.selectedFilter}
                  type={filter.type}
                  properties={filter.properties}
                />
                <p className={styles.Error}>{errors[props.selectedFilter] && errors[props.selectedFilter].message}</p>
              </React.Fragment>
            )}
          </DialogContent>
          <DialogActions>
            <Button onClick={cancelHandler} color="secondary">
              Cancel
            </Button>
            <Button onClick={backHandler} color="secondary">
              Back
            </Button>
            <Button type='submit' color="primary">
              Add Filter
            </Button>
          </DialogActions>
        </form>
      </Dialog>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    selectedFilter: state.filters.selectedFilter,
    availableFilters: state.filters.availableFilters
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onAddFilter: (filter) => dispatch(action.addFilter(filter))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(FilterDialog);