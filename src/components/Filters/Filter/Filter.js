import React from 'react';

import Chip from "@material-ui/core/Chip";

import styles from './Filter.module.css';

const Filter = (props) => {
  return (
    <Chip
      className={styles.Filter}
      label={props.label}
      onDelete={props.onDelete}
    />
  );
};

export default Filter;