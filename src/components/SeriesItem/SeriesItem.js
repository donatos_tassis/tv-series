import React, { memo } from 'react';
import { connect } from 'react-redux';
import { Element } from 'react-scroll';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Rating from '../UI/Rating/Rating';
import * as action from "../../store/actions";
import defaultImage from '../../helper/defaultImage';

import 'react-circular-progressbar/dist/styles.css';
import styles from './SeriesItem.module.css';

const SeriesItem = memo(props => {

  const imagesUrl = process.env.REACT_APP_IMAGES_BASE_URL;
  const image = imagesUrl + 'w154/' + props.data.poster_path;

  const openSeriesDetails = () => {
    props.scrollPos();
    props.fetchSeriesDetails(props.data.id, props.history.push, props.link);
    props.loadEpisodes();
  }

  return (
    <li className={styles.SeriesItem}>
      <Element name={props.className} className={props.className}/>
      <Card elevation={10} className={styles.Card}>
        <CardContent>
          <div className={styles.ImageContainer}>
            <img
              className={styles.SeriesImage}
              src={image}
              onError={defaultImage}
              onClick={openSeriesDetails}
              alt=""
            />
          </div>
          <Rating className={styles.Rating} rating={props.data.vote_average}/>
          <div className={styles.SeriesName}>
            <article><b>{props.data.name}</b></article>
          </div>
        </CardContent>
      </Card>
    </li>
  );
});

const mapDispatchToProps = dispatch => {
  return {
    fetchSeriesDetails: (id, push, link) => dispatch(action.fetchSeriesDetails(id, push, link)),
    loadEpisodes: () => dispatch(action.seasonChange()),
  }
}

export default connect(null, mapDispatchToProps)(SeriesItem);