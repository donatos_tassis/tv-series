import React, { memo } from 'react';

import { CircularProgressbar } from 'react-circular-progressbar';

const Rating = memo((props) => {
  return (
    <div className={props.className}>
      <CircularProgressbar
        styles={{
          text: {
            fontSize: '22px',
            fill: '#000000',
            fontWeight: 'bold'
          },
          trail: {
            stroke: '#b5adff',
          },
          path: {
            stroke: '#6637d4'
          },
          background: {
            fill: '#fff',
          }
        }}
        background={true}
        backgroundPadding={1}
        minValue={0}
        maxValue={10}
        text={`${props.rating} ⭐`}
        value={props.rating}
      />
    </div>
  );
});

export default Rating;