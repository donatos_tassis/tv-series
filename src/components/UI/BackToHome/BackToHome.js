import React, { memo } from 'react';

import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronCircleLeft } from '@fortawesome/free-solid-svg-icons';

import styles from './BackToHome.module.css';
import * as action from '../../../store/actions';

const BackToHome = memo((props) => {

  return (
    <div className={styles.Back}>
      <Link to={{
        pathname: '/',
        state: { element: props.id }
      }}>
        <FontAwesomeIcon
          className={styles.BackButton}
          icon={faChevronCircleLeft}
          size='3x'
          onClick={ props.onRedirect }/>
      </Link>
    </div>
  );
});

const mapDispatchToProps = dispatch => {
  return {
    onRedirect: () => dispatch(action.redirectToSeriesList(true)),
  }
}

export default connect(null, mapDispatchToProps)(BackToHome);