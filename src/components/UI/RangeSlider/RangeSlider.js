import React, {useEffect} from 'react';

import Slider from '@material-ui/core/Slider';

import styles from './RangeSlider.module.css';

const RangeSlider = (props) => {
  const value = props.watch(props.name, props.defaultValue);

  const { register, name } = props;
  useEffect(() => {
    register({name: name});
  }, [register, name]);

  const handleChange = (event, newValue) => {
    props.setValue(props.name, newValue);
  };

  return (
    <div className={styles.RangeSlider}>
      <Slider
        innerRef={register}
        name={props.name}
        min={props.min}
        max={props.max}
        marks={props.marks}
        value={value}
        defaultValue={props.defaultValue}
        onChange={handleChange}
        track='inverted'
        valueLabelDisplay="auto"
        aria-labelledby="range-slider"
      />
    </div>
  );
}

export default RangeSlider;