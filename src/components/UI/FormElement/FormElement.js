import React from 'react';

import RangeSlider from '../RangeSlider/RangeSlider';
import { CheckBoxSelection, Inject, MultiSelectComponent } from '@syncfusion/ej2-react-dropdowns';

import '@syncfusion/ej2-react-dropdowns/styles/material.css';
import '@syncfusion/ej2-inputs/styles/material.css';
import '@syncfusion/ej2-buttons/styles/material.css';
import '@syncfusion/ej2-base/styles/material.css';

import styles from './FormElement.module.css';

const FormElement =(props) => {
  let inputElement = null;

  switch (props.type) {
    case ('textarea'):
      inputElement = (
        <textarea
          name={props.name}
          ref={props.register(props.validation)}
          className={styles.FormElement}
          {...props.properties}
        />);
      break;
    case ('select'):
      inputElement = (
        <select
          name={props.name}
          ref={props.register(props.validation)}
          className={styles.FormElement}
        >
          {props.properties.options.map(option => {
            return <option key={option.value} value={option.value}>{option.displayValue}</option>;
          })}
        </select>
      );
      break;
    case ('range-slider'):
      inputElement = (<RangeSlider
            name={props.name}
            register={props.register}
            watch={props.watch}
            setValue={props.setValue}
            validation={props.validation}
            className={styles.FormElement}
            {...props.properties}
          />
      );
      break;
    case ('multi-select'):
      inputElement = (<MultiSelectComponent
          name={props.name}
          ref={props.register(props.validation)}
          dataSource={props.properties.options}
          {...props.properties}
        >
          <Inject services={[CheckBoxSelection]}/>
        </MultiSelectComponent>
      )
      break;
    default:
      inputElement = (<input
        name={props.name}
        ref={props.register(props.validation)}
        className={styles.FormElement}
        {...props.properties}
      />);
  }

  return (
    <div className={styles.FormElementBlock}>
      <label className={styles.Label}>{props.properties.label}</label>
      {inputElement}
    </div>
  );
};

export default FormElement;