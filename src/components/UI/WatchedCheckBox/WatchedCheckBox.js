import React from 'react';

import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';
import VisibilityIcon from '@material-ui/icons/Visibility';

const WatchedCheckBox = (props) => {
  const {id, value} = props;

  const toggleWatched = (key, value) => {
    // ToDo: need to make a request to the API (only when user is authenticated) to change the watched value
    console.log(key, value);
  }

  return (
    <FormControlLabel
      label=''
      labelPlacement='top'
      onChange={() => toggleWatched(id, value)}
      control={
        <Checkbox
          checked={value}
          name={`watched-${id}`}
          icon={<VisibilityOffIcon />}
          checkedIcon={<VisibilityIcon />}
        />
      }
    />
  );
};

export default WatchedCheckBox;