import React, { useState, useEffect } from 'react';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';

const ErrorDialog = (props) => {
  console.log(props);
  const [open, setOpen] = useState(props.open);

  useEffect(() => {
    setOpen(open);
  }, [open])

  const handleClose = (redirect) => {
    setOpen(false);
    if (redirect)
      props.history.push(redirect);
  };

  return (
    <React.Fragment>
      <Dialog
        open={open}
        onClose={() => handleClose(props.to)}
      >
        <DialogTitle>Error</DialogTitle>
        <DialogContent>
          <DialogContentText>
            {props.error && props.error}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <button onClick={() => handleClose(props.to)}>Close</button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
};

export default ErrorDialog;