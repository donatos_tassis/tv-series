import React, { useRef } from 'react';
import { Switch, Route } from 'react-router';

import Layout from './hoc/Layout/Layout';
import SeriesList from './containers/SeriesList/SeriesList';
import SeriesDetails from './containers/SeriesDetails/SeriesDetails';

function App() {
  const mainRef = useRef(null);

  return (
    <Layout mainRef={mainRef}>
      <Switch>
        <Route path='/series/:id' render={(props) => <SeriesDetails {...props}/>} />
        <Route path='/' exact render={(props) => <SeriesList {...props} mainRef={mainRef}/>} />
      </Switch>
    </Layout>
   );
}

export default App;
