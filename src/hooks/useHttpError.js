import { useState, useEffect } from 'react';

export default useHttpClient => {
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  const reqInterceptor = useHttpClient.interceptors.request.use(request => {
    setError(null);
    return request;
  });

  const resInterceptor = useHttpClient.interceptors.response.use(response => response, error => {
    setError(error.response.data.status_message);
    setLoading(false);

    return Promise.reject(error);
  });

  useEffect(() => {
    return () => {
      useHttpClient.interceptors.request.eject(reqInterceptor);
      useHttpClient.interceptors.response.eject(resInterceptor);
    }
  }, [reqInterceptor, resInterceptor]);

  const errorConfirmedHandler = () => {
    setError(null);
    setLoading(true);
  }

  return [error, loading, errorConfirmedHandler];
}