import axios from '../../axios/axios-api';
import * as types from './actionTypes';

export const selectFilterStart = (selectedFilter) => {
  return {
    type: types.SELECT_FILTER_START,
    selectedFilter: selectedFilter
  }
}

export const selectFilterFinish = () => {
  return {
    type: types.SELECT_FILTER_FINISH,
  }
}

export const addFilter = (filter) => {
  return {
    type: types.ADD_FILTER,
    filter: filter
  }
}

export const removeFilter = (id) => {
  return {
    type: types.REMOVE_FILTER,
    id: id
  }
}

export const clearFilters = () => {
  return {
    type: types.CLEAR_FILTERS
  }
}

export const fetchGenreFiltersInit = () => {
  return {
    type: types.FETCH_GENRE_FILTERS_INIT
  }
}

export const fetchGenreFiltersSuccess = (data) => {
  return {
    type: types.FETCH_GENRE_FILTERS_SUCCESS,
    data: data
  }
}

export const fetchGenreFiltersFail = (error) => {
  return {
    type: types.FETCH_GENRE_FILTERS_FAIL,
    error: error
  }
}

export const fetchGenreFilters = () => {
  return dispatch => {
    dispatch(fetchGenreFiltersInit());
    axios.get('/series/genre/list')
      .then(response => {
        let genres = [];
        response.data.genres.forEach(genre => {
          genres.push({
            value: genre.id,
            displayValue: genre.name
          });
        });
        dispatch(fetchGenreFiltersSuccess(genres));
      })
      .catch(error => {
        console.log('error', error);
        dispatch(fetchGenreFiltersFail(error))
      })
  }
}

export const applyFiltersStart = (reapply) => {
  return {
    type: types.APPLY_FILTERS_START,
    reapply: reapply
  }
}

export const applyFiltersSuccess = (filteredResults, pages, currentPage, totalResults, reapply) => {
  return {
    type: types.APPLY_FILTERS_SUCCESS,
    filtered: true,
    seriesList: filteredResults,
    pages: pages,
    currentPage: currentPage,
    totalResults: totalResults,
    reapply: reapply
  }
}

export const applyFiltersFail = (error) => {
  return {
    type: types.APPLY_FILTERS_FAIL,
    error: error
  }
}

export const applyFilters = (filters, page, reapply = false) => {
  return dispatch => {
    dispatch(applyFiltersStart(reapply));
    axios.post('/series/filter', filters,{params: {page: page}})
      .then(response => {
        if(response.data.results.length === 0 && page < response.data.total_pages) {
          dispatch(applyFilters(filters, ++page));
        } else {
          dispatch(applyFiltersSuccess(
            response.data.results,
            response.data.total_pages,
            response.data.page,
            response.data.total_results,
            reapply
          ));
        }
      })
      .catch(error => {
        console.log(error);
        dispatch(applyFiltersFail(error));
      })
  }
}