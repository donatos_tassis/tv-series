import * as types from './actionTypes';

export const toggleFilters = () => {
  return {
    type: types.TOGGLE_FILTERS,
  }
}