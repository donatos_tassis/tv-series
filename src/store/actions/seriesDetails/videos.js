import axios from '../../../axios/axios-api';

import * as types from '../actionTypes';

export const seasonChange = () => {
  return {
    type: types.SEASON_CHANGE
  }
}

export const fetchVideosInit = () => {
  return {
    type: types.FETCH_VIDEOS_INIT
  }
}

export const fetchVideosFail = (error) => {
  return {
    type: types.FETCH_VIDEOS_FAIL,
    error: error
  }
}

export const fetchVideosSuccess = (payload) => {
  return {
    type: types.FETCH_VIDEOS_SUCCESS,
    payload: payload
  }
}

export const clearVideos = () => {
  return {
    type: types.FETCH_VIDEOS_RESET
  }
}

export const fetchVideos = (id) => {
  return dispatch => {
    dispatch(fetchVideosInit());
    axios.get('/series/' + id.toString() + '/videos')
      .then(response => {
        dispatch(fetchVideosSuccess(response.data));
      })
      .catch(error => {
        dispatch(fetchVideosFail(error.response.data));
      })
  }
}