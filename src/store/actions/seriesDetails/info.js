import axios from '../../../axios/axios-api';

import * as types from '../actionTypes';

export const seriesDetailsInit = () => {
  return {
    type: types.SERIES_DETAILS_INIT
  }
}

export const seriesDetailsFail = (error) => {
  return {
    type: types.SERIES_DETAILS_FAIL,
    error: error
  }
}

export const seriesDetailsSuccess = (payload) => {
  return {
    type: types.SERIES_DETAILS_SUCCESS,
    payload: payload
  }
}

export const fetchSeriesDetails = (id, push = null, link = null) => {
  return dispatch => {
    dispatch(seriesDetailsInit());
    axios.get('/series/' + id.toString())
      .then(response => {
        dispatch(seriesDetailsSuccess(response.data));
        if (push)
          push(link.pathname, link.state);
      })
      .catch(error => {
        dispatch(seriesDetailsFail(error.response.data));
      })
  }
}