import axios from '../../../axios/axios-api';

import * as types from '../actionTypes';

export const seasonChange = () => {
  return {
    type: types.SEASON_CHANGE
  }
}

export const seasonEpisodesInit = () => {
  return {
    type: types.SEASON_EPISODES_INIT
  }
}

export const seasonEpisodesFail = (error) => {
  return {
    type: types.SEASON_EPISODES_FAIL,
    error: error
  }
}

export const seasonEpisodesSuccess = (payload) => {
  return {
    type: types.SEASON_EPISODES_SUCCESS,
    payload: payload
  }
}

export const fetchSeasonEpisodes = (id, season) => {
  return dispatch => {
    dispatch(seasonEpisodesInit());
    axios.get('/series/' + id.toString() + '/season/' + season.toString())
      .then(response => {
        dispatch(seasonEpisodesSuccess(response.data));
      })
      .catch(error => {
        dispatch(seasonEpisodesFail(error.response.data));
      })
  }
}