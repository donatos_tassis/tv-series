import axios from '../../axios/axios-api';

import * as types from './actionTypes';

export const fetchPopularSeriesStart = () => {
  return {
    type: types.FETCH_SERIES_START,
  }
}

export const fetchPopularSeriesSuccess = (seriesList, pages, currentPage, totalResults) => {
  return {
    type: types.FETCH_SERIES_SUCCESS,
    filtered: false,
    seriesList: seriesList,
    pages: pages,
    currentPage: currentPage,
    totalResults: totalResults
  }
}

export const fetchPopularSeriesFail = (error) => {
  return {
    type: types.FETCH_SERIES_FAIL,
    error: error
  }
}

export const fetchPopularSeriesReset = () => {
  return {
    type: types.FETCH_SERIES_RESET
  }
}

export const fetchPopularSeries = (page) => {
  return dispatch => {
    dispatch(fetchPopularSeriesStart());
    axios.get('/series/popular', {params: {page: page}})
      .then(response => {
        dispatch(fetchPopularSeriesSuccess(
          response.data.results,
          response.data.total_pages,
          response.data.page,
          response.data.total_results));
      })
      .catch(error => {
        console.log('error', error);
        dispatch(fetchPopularSeriesFail(error));
      })
  }
}

export const redirectToSeriesList = (redirected) => {
  return {
    type: types.REDIRECT_TO_SERIES_LIST,
    redirected: redirected
  }
}

export const nextPage = (page) => {
  return {
    type: types.NEXT_PAGE,
    nextPage: page
  }
}

export const filtered = (page, filtered) => {
  return {
    type: types.FILTERED,
    page: page,
    filtered: filtered
  }
}

export const setScrollPos = (scrollPos) => {
  return {
    type: types.STORE_SCROLL_Y,
    scrollPos: scrollPos
  }
}