import * as types from '../actions/actionTypes';

const initialState = {
  seriesList: [],
  error: null,
  pages: 0,
  currentPage: 1,
  nextPage: 1,
  totalResults: 0,
  filtered: false,
  redirected: false,
  scrollTop: true,
  scrollPos: 0
}

const reducer = (state = initialState, action) => {
  let seriesList = [];
  let nextPage = state.nextPage;
  let scrollTop = state.scrollTop;
  switch (action.type) {
    case types.FETCH_SERIES_RESET:
      return {...initialState}
    case types.FETCH_SERIES_START:
      return {...state, error: null, redirected: false, scrollTop: false}
    case types.FETCH_SERIES_FAIL:
      return {...initialState, error: action.error}
    case types.FETCH_SERIES_SUCCESS:
      if(action.reapply || state.filtered !== action.filtered) {
        seriesList = action.seriesList;
        nextPage = 1;
        scrollTop = true;
      }
      else if (state.filtered === action.filtered)
        seriesList = state.seriesList.concat(action.seriesList);
      return {
        ...state,
        seriesList: seriesList,
        error: null,
        pages: action.pages,
        currentPage: action.currentPage,
        nextPage: nextPage,
        totalResults: action.totalResults,
        filtered: action.filtered,
        redirected: false,
        scrollTop: scrollTop
      }
    case types.REDIRECT_TO_SERIES_LIST:
      return { ...state, redirected: action.redirected }
    case types.APPLY_FILTERS_START:
      return {...state, error: null, redirected: false, scrollTop: false}
    case types.APPLY_FILTERS_FAIL:
      return {...initialState, error: action.error}
    case types.APPLY_FILTERS_SUCCESS:
      if (action.reapply || state.filtered !== action.filtered) {
        seriesList = action.seriesList;
        nextPage = 1;
        scrollTop = true;
      }
      else if (state.filtered === action.filtered)
        seriesList = state.seriesList.concat(action.seriesList);
      return {
        ...state,
        seriesList: seriesList,
        error: null,
        pages: action.pages,
        currentPage: action.currentPage,
        nextPage: nextPage,
        totalResults: action.totalResults,
        filtered: action.filtered,
        redirected: false,
        scrollTop: scrollTop
      }
    case types.NEXT_PAGE:
      return {...state, nextPage: action.nextPage}
    case types.FILTERED:
      return {
        ...state,
        seriesList: [],
        currentPage: action.page,
        nextPage: action.page,
        filtered: action.filtered,
        scrollTop: true
      }
    case types.STORE_SCROLL_Y:
      return {...state, scrollPos: action.scrollPos}
    default: return state;
  }
}

export default reducer;