import * as types from '../actions/actionTypes';

const initialState = {
  showFilters: false,
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.TOGGLE_FILTERS:
      return {showFilters: !state.showFilters};
    default: return state;
  }
}

export default reducer;