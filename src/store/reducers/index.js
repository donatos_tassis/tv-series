import { combineReducers } from 'redux';
import globalReducer from './global';
import seriesListReducer from './seriesList';
import filtersReducer from './filters/filters';
import seriesDetailsReducer from './seriesDetails/index';

const rootReducer = combineReducers({
  global: globalReducer,
  seriesList: seriesListReducer,
  filters: filtersReducer,
  seriesDetails: seriesDetailsReducer
});

export default rootReducer;