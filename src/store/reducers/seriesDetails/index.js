import { combineReducers } from 'redux';
import infoReducer from './info';
import seasonReducer from './season';
import videosReducer from './videos';

const seriesDetails = combineReducers({
  info: infoReducer,
  season: seasonReducer,
  videos: videosReducer
});

export default seriesDetails;