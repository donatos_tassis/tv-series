import * as types from '../../actions/actionTypes';

const initialState = {
  data: {},
  error: null,
  loading: true,
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SERIES_DETAILS_INIT:
      return { ...state, loading: true, error: null };
    case types.SERIES_DETAILS_FAIL:
      return { ...state, data: {}, error: action.error, loading: false };
    case types.SERIES_DETAILS_SUCCESS:
      return { ...state, data: action.payload, error: null, loading : false };
    default: return state;
  }
}

export default reducer;