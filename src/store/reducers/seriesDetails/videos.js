import * as types from '../../actions/actionTypes';

const initialState = {
  data: null,
  error: null,
  loading: true,
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.FETCH_VIDEOS_INIT:
      return {...state, data: null, error: null, loading: true};
    case types.FETCH_VIDEOS_FAIL:
      return {...state, data: null, error: action.error, loading: false};
    case types.FETCH_VIDEOS_SUCCESS:
      return {...state, data: action.payload, error: null, loading: false};
    case types.FETCH_VIDEOS_RESET:
      return {...state, data: null, loading: true}
    default: return state;
  }
}

export default reducer;