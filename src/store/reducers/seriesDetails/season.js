import * as types from '../../actions/actionTypes';

const initialState = {
  data: null,
  error: null,
  loading: true,
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SEASON_CHANGE:
      return {...state, loading: true }
    case types.SEASON_EPISODES_INIT:
      return {...state, data: null, error: null, loading: true};
    case types.SEASON_EPISODES_FAIL:
      return {...state, data: null, errors: action.error, loading: false};
    case types.SEASON_EPISODES_SUCCESS:
      return {...state, data: action.payload, error: null, loading: false};
    default: return state;
  }
}

export default reducer;