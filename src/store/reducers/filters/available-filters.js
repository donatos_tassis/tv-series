const filters = {
  query: {
    displayName: 'Title',
    description: 'Filter series by title.',
    type: 'input',
    properties: {
      defaultValue: '',
      placeholder: 'Search...'
    },
    validation: {
      required: {
        value: true,
        message: 'A search value should be provided'
      }
    }
  },
  rating: {
    displayName: 'Rating',
    description: 'Filter series by rating.',
    type: 'range-slider',
    properties: {
      type: 'range',
      label: 'Rating over selected value',
      min: 0,
      max: 10,
      defaultValue: 5,
      marks: [
        {value: 0, label: 0},
        {value: 1, label: 1},
        {value: 2, label: 2},
        {value: 3, label: 3},
        {value: 4, label: 4},
        {value: 5, label: 5},
        {value: 6, label: 6},
        {value: 7, label: 7},
        {value: 8, label: 8},
        {value: 9, label: 9},
        {value: 10, label: 10}
      ]
    },
    validation: {

    }
  },
  with_genres: {
    displayName: 'Genre',
    description: 'Filter series by genre.',
    type: 'multi-select',
    properties: {
      placeholder: 'Select categories',
      filterBarPlaceholder: 'Search categories',
      showDropDownIcon: true,
      showSelectAll: true,
      mode: 'CheckBox',
      fields: { text: 'displayValue', value: 'value' },
      defaultValue: '',
      label: 'Genre',
      options: []
    },
    validation: {
      required: {
        value: true,
        message: 'You should pick at least one genre'
      }
    }
  },
  sort_by: {
    displayName: 'Sort by',
    description: 'Sorts the list of series.',
    type: 'select',
    properties: {
      defaultValue: '',
      label: 'Sort By',
      options: [
        { value: 'popularity.desc', displayValue: 'Popularity Descending' },
        { value: 'popularity.asc', displayValue: 'Popularity Ascending' },
        { value: 'vote_average.desc', displayValue: 'Rating Descending' },
        { value: 'vote_average.asc', displayValue: 'Rating Ascending' },
        { value: 'first_air_date.desc', displayValue: 'Release Date Descending' },
        { value: 'first_air_date.asc', displayValue: 'Release Date Ascending' }
      ]
    },
    validation: {
      required: {
        value: true,
        message: 'You should select one of those options'
      }
    }
  }
}

export default filters;