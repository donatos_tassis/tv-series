import * as types from '../../actions/actionTypes';
import availableFilters from './available-filters';

const clone = require('rfdc')();

const initialState = {
  filters: [],
  availableFilters: availableFilters,
  selectedFilter: null,
  modified: false
}

let modified = null;

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SELECT_FILTER_START:
      return {...state, selectedFilter: action.selectedFilter}
    case types.SELECT_FILTER_FINISH:
      return {...state, selectedFilter: null}
    case types.ADD_FILTER:
      return {...state, selectedFilter: null, filters: updatedFilters(state.filters, action.filter), modified: modified}
    case types.REMOVE_FILTER:
      return {...state, selectedFilter: null, filters: state.filters.filter(filter => {
          return filter.id !== action.id;
        })}
    case types.CLEAR_FILTERS:
      return {...state, selectedFilter: null, filters: []}
    case types.FETCH_GENRE_FILTERS_INIT:
      return state;
    case types.FETCH_GENRE_FILTERS_FAIL:
      return {...state, availableFilters: initialState.availableFilters }
    case types.FETCH_GENRE_FILTERS_SUCCESS:
      const clonedState = clone(state);
      clonedState.availableFilters.with_genres.properties.options = action.data;
      return clonedState;
    default: return state;
  }
}

const updatedFilters = (filters, filter) => {
    modified = false;
    for (let key in filters) {
      if (filters[key].id === filter.id) {
        filters[key].value = filter.value;
        filters[key].displayValue = filter.displayValue;
        modified = true;
        return filters;
      }
    }

    return filters.concat(filter);
}

export default reducer;