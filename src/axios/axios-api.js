import axios from 'axios';

const instance = axios.create({
  baseURL: process.env.REACT_APP_API_BASE_URL
});

instance.interceptors.request.use(request => {
  request.params = {
    ...request.params,
    'XDEBUG_SESSION_START': 'PHPSTORM'
  }

  return request;
})

export default instance;