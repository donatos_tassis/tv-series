import { animateScroll as scroll } from 'react-scroll';

const scrollTo = (containerId, duration, toY = 0) => {
  scroll.scrollTo(toY, {
    duration: duration,
    smooth: true,
    containerId: containerId
  });
}

export default scrollTo;