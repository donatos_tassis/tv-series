import noImg from "../assets/images/no-img.png";

const defaultImage = (event) => {
  event.target.onerror = null;
  event.target.src = noImg;
}

export default defaultImage;