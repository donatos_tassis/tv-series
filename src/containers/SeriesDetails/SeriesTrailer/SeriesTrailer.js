import React, { useEffect, memo } from 'react';
import { connect } from 'react-redux';

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import ReactPlayer from 'react-player'
import Spinner from '../../../components/UI/Spinner/Spinner';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import { withStyles } from '@material-ui/core/styles';

import * as action from '../../../store/actions';
import * as styles from './SeriesTrailer.module.css';

const SeriesTrailer = memo((props) => {

  const { loading, fetchVideos, match, error, open, data } = props;
  useEffect(() => {
    if (loading && open)
      fetchVideos(match.params.id);
  }, [loading, fetchVideos, match, open]);

  const dialogStyles = (theme) => ({
    root: {
      margin: 0,
      backgroundColor: '#000',
      padding: theme.spacing(3),
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(1),
      top: theme.spacing(0),
      color: theme.palette.grey[500],
    },
  });

  const DialogTitle = withStyles(dialogStyles)((props) => {
    const { children, classes, onClose, ...other } = props;
    return (
      <MuiDialogTitle disableTypography className={classes.root} {...other}>
        {onClose ? (
          <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
            <CloseIcon />
          </IconButton>
        ) : null}
      </MuiDialogTitle>
    );
  });

  return (
    !error &&
    <div>
      <Dialog open={open} onClose={props.close} maxWidth='md' fullWidth>
        <DialogTitle onClose={props.close} />
        <DialogContent style={{
          height: (data && data.results.length > 0) ? data.results[0].size : '500px',
          maxHeight: '60vh',
          padding: '0',
          overflow: 'hidden',
          position: 'relative',
          backgroundColor: 'rgba(0, 0, 0, 0.8)'
        }}>
          {
            loading ? <Spinner /> :
              (data.results.length > 0) ?
                <ReactPlayer
                  url={`https://www.youtube.com/watch?v=${data.results[0].key}`}
                  width='100%'
                  height='100%'
                  playing
                  controls
                /> : <p className={styles.NoTrailer}>No trailer available</p>
          }
        </DialogContent>
      </Dialog>
    </div>
  );
});

const mapStateToProps = state => {
  return {
    data: state.seriesDetails.videos.data,
    loading: state.seriesDetails.videos.loading,
    error: state.seriesDetails.videos.error,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchVideos: (id) => dispatch(action.fetchVideos(id)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SeriesTrailer);