import React, { useEffect, memo } from 'react';

import {connect} from 'react-redux';
import EpisodesTable from '../../../components/SeriesDetails/EpisodesTable/EpisodesTable';
import Spinner from '../../../components/UI/Spinner/Spinner';

import * as action from '../../../store/actions';
import styles from './SeriesEpisodes.module.css';

const SeriesEpisodes = memo((props) => {

  const { data, seasonData, match, seasonIndex, fetchSeasonEpisodes, loadingInfo, loadingEpisodes, errorInfo } = props;

  useEffect(() => {
    if (!loadingInfo && loadingEpisodes && !errorInfo) {
      fetchSeasonEpisodes(match.params.id, data.seasons[seasonIndex].season_number);
    }
  }, [loadingEpisodes, loadingInfo, seasonIndex, fetchSeasonEpisodes, match, data, errorInfo]);

  const cssStyle = (opacity, width = 'auto', bold = false, textAlignCenter = false, properties = {}) => {
    let style = {
      ...properties,
      width: width,
      backgroundColor: `rgba(62,14,139,${opacity})`
    }

    if (bold)
      style.fontWeight = 'bold'

    if (textAlignCenter)
      style.textAlign = 'center';

    return style;
  }

  const columns = [
    { id: 'episode_number', label: 'Episode', style: cssStyle(0.25, '2%',true, true)},
    { id: 'name', label: 'Title', style: cssStyle(0.1, '24%',true, true)},
    { id: 'air_date', label: 'Air Date', style: cssStyle(0.25, '10%',false, true)},
    { id: 'overview', label: 'Overview', style: cssStyle(0.1, '60%')},
    { id: 'watched', label: 'Watched', style: cssStyle(0.25, '2%')},
  ];

  const rows = () => seasonData.episodes.map(episode => {
    return {
      episode_number: episode.episode_number,
      name: episode.name,
      air_date: episode.air_date,
      overview: episode.overview,
      watched: false // ToDo : need to be replaces with actual value coming from API
    }
  });

  return (
    (!props.errorEpisodes && !props.errorInfo) &&
    <div className={styles.SeriesEpisodes}>
      {loadingEpisodes ? <Spinner /> :
      <EpisodesTable columns={columns} rows={rows()} />}
    </div>
  );
});

const mapStateToProps = state => {
  return {
    data: state.seriesDetails.info.data,
    seasonData: state.seriesDetails.season.data,
    loadingInfo: state.seriesDetails.info.loading,
    errorInfo: state.seriesDetails.info.error,
    loadingEpisodes: state.seriesDetails.season.loading,
    errorEpisodes: state.seriesDetails.season.error,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onRedirect: () => dispatch(action.redirectToSeriesList(true)),
    fetchSeasonEpisodes: (id, season) => dispatch(action.fetchSeasonEpisodes(id, season))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SeriesEpisodes);