import React, {memo, useState, useEffect} from 'react';

import {connect} from 'react-redux';
import SeriesSeasons from '../../../components/SeriesDetails/SeriesSeasons/SeriesSeasons';
import defaultImage from '../../../helper/defaultImage';
import Tooltip from '@material-ui/core/Tooltip';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faHeart, faPlay} from '@fortawesome/free-solid-svg-icons';
import Spinner from '../../../components/UI/Spinner/Spinner';
import SeriesTrailer from '../SeriesTrailer/SeriesTrailer';
import Rating from '../../../components/UI/Rating/Rating';

import * as action from '../../../store/actions';
import styles from './SeriesInfo.module.css';

const SeriesInfo = memo((props) => {
  const { data, season, setSeason, fetchSeriesDetails, error, loading, match, clearVideos } = props;
  const imagesUrl = process.env.REACT_APP_IMAGES_BASE_URL;
  const [openTrailer, setOpenTrailer] = useState(false);

  const openTrailerDialog = () => {
    setOpenTrailer(true);
  };

  const closeTrailerDialog = () => {
    clearVideos();
    setOpenTrailer(false);
  };

  useEffect(() => {
    if (loading) {
      fetchSeriesDetails(match.params.id);
    }
  }, [loading, fetchSeriesDetails, match, data]);

  const genreNames = () => {
    const genres = data.genres.map(genre => {
      return genre.name;
    });

    return genres.join(', ');
  }

  const airYear = () => {
    let airYear = '';
    if (data.first_air_date)
      airYear = '('+ data.first_air_date.substring(0, 4) + ')';

    return airYear;
  }

  const backdrop = imagesUrl + 'original/' + data.backdrop_path;
  const image = imagesUrl + 'w154/' + data.poster_path;

  if (loading)
    return <div className={styles.Loader}><Spinner /></div>

  return (
    !error &&
    <React.Fragment>
      <SeriesTrailer {...props} open={openTrailer} close={closeTrailerDialog}/>
      <div className={styles.SeriesInfo}>
        <div className={styles.BackGround} style={{backgroundImage: `url(${backdrop})`}}/>
        <img className={styles.SeriesImage} src={image} onError={defaultImage} alt=''/>
        <div className={styles.SeriesTitle}>
          <h2>{data.name} {airYear()}</h2>
          <article>{genreNames()}</article>
        </div>
        <div className={styles.OverviewTitle}><h3>Overview</h3></div>
        <div className={styles.Overview}><p>{data.overview}</p></div>
        <div className={styles.ActionButtons}>
          <span>
            <Tooltip title='Add to Favorites'>
              <button>
                <FontAwesomeIcon icon={faHeart} size='1x'/>
              </button>
            </Tooltip>
          </span>
          <span>
            <Tooltip title='Watch Trailer'>
              <button onClick={openTrailerDialog}>
                <FontAwesomeIcon icon={faPlay} size='1x'/>
              </button>
            </Tooltip>
          </span>
        </div>
        <Rating className={styles.Rating} rating={props.data.vote_average}/>
      </div>
      <SeriesSeasons seasons={data.seasons} season={season} setSeason={setSeason} />
    </React.Fragment>
  );
});

const mapStateToProps = state => {
  return {
    data: state.seriesDetails.info.data,
    loading: state.seriesDetails.info.loading,
    error: state.seriesDetails.info.error,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchSeriesDetails: (id) => dispatch(action.fetchSeriesDetails(id)),
    clearVideos: () => dispatch(action.clearVideos())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SeriesInfo);