import React, { useState, memo, useEffect } from 'react';
import { connect } from 'react-redux';

import withHttpError from '../../hoc/withHttpError/withHttpError';
import axios from '../../axios/axios-api';
import SeriesInfo from './SeriesInfo/SeriesInfo';
import SeriesEpisodes from './SeriesEpisodes/SeriesEpisodes';

import * as action from '../../store/actions';
import styles from './SeriesDetails.module.css';

const SeriesDetails = memo((props) => {

  useEffect(() => {
    if (props.showFilters)
      props.toggleFilters()
  })

  const [seasonIndex, setSeasonIndex] = useState(0);

  return (
    <div className={styles.SeriesDetails}>
      <SeriesInfo {...props} season={seasonIndex} setSeason={setSeasonIndex} />
      <SeriesEpisodes {...props} seasonIndex={seasonIndex} />
    </div>
  );
});

const mapStateToProps = state => {
  return {
    showFilters: state.global.showFilters
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onRedirect: () => dispatch(action.redirectToSeriesList(true)),
    toggleFilters: () => dispatch(action.toggleFilters()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(withHttpError(SeriesDetails, axios));