import React, {useCallback, useEffect, useState} from 'react';
import { connect } from 'react-redux';

import FilterSelectionDialog from '../../components/Filters/FilterSelectionDialog/FilterSelectionDialog';
import FilterDialog from '../../components/Filters/FilterDialog/FilterDialog';
import Filter from '../../components/Filters/Filter/Filter'
import Fab from '@material-ui/core/Fab';
import Tooltip from '@material-ui/core/Tooltip';
import ScrollMenu from 'react-horizontal-scrolling-menu';
import { FontAwesomeIcon as FaIcon } from '@fortawesome/react-fontawesome'
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/AddOutlined';
import {faAngleLeft, faAngleRight, faSearch} from '@fortawesome/free-solid-svg-icons';
import * as action from "../../store/actions";

import styles from "./Filters.module.css";
import HideComponent from "../../hoc/HideComponent/HideComponent";

const Filters = props => {
  const [ openFilterSelectionDialog, setOpenFilterSelectionDialog ] = useState(false);
  const [ openFilterDialog, setOpenFilterDialog ] = useState(false);
  const [ tags, setTags ] = useState([]);

  const Menu = (list, deleteFilter) =>
    list.map(filter => {
      return (<Filter
        key={filter.id}
        label={filter.title + ' : ' + filter.displayValue}
        onDelete={() => deleteFilter(filter.id)}
      />);
    });

  const openFilterSelectionDialogHandler = () => {
    setOpenFilterSelectionDialog(true);
  }

  const closeFilterSelectionDialogHandler = () => {
    setOpenFilterSelectionDialog(false);
  }

  const openFilterDialogHandler = (selectedFilter) => {
    if(selectedFilter === 'with_genres') {
      props.fetchGenreFilters();
    }
    props.onSelectedFilter(selectedFilter);
    setOpenFilterDialog(true);
  }

  const closeFilterDialogHandler = () => {
    props.cancelFilterSelection();
    setOpenFilterDialog(false);
    setOpenFilterSelectionDialog(false);
  }

  const backToFilterSelectionDialogHandler = () => {
    props.cancelFilterSelection();
    setOpenFilterDialog(false);
    setOpenFilterSelectionDialog(true);
  }

  const submitFilters = event => {
    props.applyFilters(props.filters, 1);
  }

  const { onRemoveFilter, filters, unFiltered } = props;
  const deleteFilter = useCallback((id) => {
    if(filters.length === 1) {  // if it is the last filter then reload the popular series
      unFiltered();
    }

    onRemoveFilter(id);
  }, [onRemoveFilter, filters, unFiltered]);

  const clearFilters = () => {
    props.onClearFilters();
    props.unFiltered();
  }

  useEffect(() => {
    setTags(Menu(props.filters.slice(0, props.filters.length), deleteFilter));
  }, [props.filters, deleteFilter, props.modifiedFilter])

  return (
    <React.Fragment>
      <FilterSelectionDialog isOpen={openFilterSelectionDialog} close={closeFilterSelectionDialogHandler} next={openFilterDialogHandler}/>
      <FilterDialog isOpen={openFilterDialog} close={closeFilterDialogHandler} back={backToFilterSelectionDialogHandler}/>
      <HideComponent {...props} direction='down' reverse={true} init={props.init} resetInit={props.resetInit}>
        <div className={styles.Filters}>
          <div className={styles.ButtonsLeft}>
            <Tooltip title='Add Filter'>
              <Fab onClick={openFilterSelectionDialogHandler} color='primary' size='small'><AddIcon/></Fab>
            </Tooltip>
          </div>
          <div className={styles.Tags}>
            <ScrollMenu
              data={tags}
              alignCenter={false}
              arrowLeft={<FaIcon icon={faAngleLeft} />}
              arrowRight={<FaIcon icon={faAngleRight}/>}
            />
          </div>
          <div className={styles.ButtonsRight}>
            <Tooltip title='Clear Filters'>
              <span>
                <Fab
                  disabled={props.filters.length === 0 }
                  onClick={clearFilters}
                  variant='round'
                  size='small'
                  color='secondary'
                  children={<DeleteIcon />}
                />
              </span>
            </Tooltip>
            <Tooltip title='Apply Filters'>
              <span>
                <Fab
                  disabled={props.filters.length === 0 }
                  onClick={submitFilters}
                  variant='round'
                  size='small'
                  color='primary'
                  children={<FaIcon icon={faSearch}/>}
                />
              </span>
            </Tooltip>
          </div>
        </div>
      </HideComponent>
    </React.Fragment>
  );
};

const mapStateToProps = state => {
  return {
    nextPage: state.seriesList.nextPage,
    filters: state.filters.filters,
    availableFilters: state.filters.availableFilters,
    modifiedFilter: state.filters.modified,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onSelectedFilter: (selectedFilter) => dispatch(action.selectFilterStart(selectedFilter)),
    onRemoveFilter: (id) => dispatch(action.removeFilter(id)),
    onClearFilters: () => dispatch(action.clearFilters()),
    unFiltered: () => dispatch(action.filtered(1,false)),
    cancelFilterSelection: () => dispatch(action.selectFilterFinish()),
    fetchGenreFilters: () => dispatch(action.fetchGenreFilters()),
    applyFilters: (filters, page) => dispatch(action.applyFilters(filters, page, true)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Filters);