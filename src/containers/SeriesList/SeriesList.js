import React, { useEffect, memo, createRef } from 'react';
import { connect } from 'react-redux';

import scrollTo from '../../helper/scrollTo';
import SeriesItem from '../../components/SeriesItem/SeriesItem';
import Spinner from '../../components/UI/Spinner/Spinner';
import InfiniteScroll from 'react-infinite-scroll-component';

import * as action from '../../store/actions';
import styles from './SeriesList.module.css';

const SeriesList = memo(props => {

  const seriesRef = createRef();
  const {fetchPopularSeries, fetchFilteredSeries, nextPage, scrollTop, filtered, filters, redirected, onRedirect, mainRef} = props;

  useEffect(() => {
    if (!redirected) {
      if (scrollTop)
        scrollTo(mainRef.current.id, 1000);
      if (filtered) {
        if (nextPage > 1) // 1st filtering page is already been loaded by the action
          fetchFilteredSeries(filters, nextPage);
      } else
        fetchPopularSeries(nextPage);
    }
  }, [fetchPopularSeries, fetchFilteredSeries, nextPage, filtered]);

  useEffect(() => {
    if (redirected) {
      onRedirect(false);
      const element = '.series-' + props.location.state.element;
      const seriesItem = seriesRef.current.querySelector(element);
      if (seriesItem) {
        scrollTo(mainRef.current.id, 0, props.scrollPos);
      } else {
        fetchPopularSeries(nextPage);
      }
    }
  }, [redirected]);

  const nextPageHandler = event => {
    props.onNextPage(props.currentPage + 1);
  }

  const storeScrollPos = () => {
    props.setScrollPos(mainRef.current.scrollTop);
  }

  const seriesList = props.seriesList.map(seriesItem => {
    return (
      <SeriesItem
        {...props}
        key={seriesItem.id}
        data={seriesItem}
        scrollPos={storeScrollPos}
        link={{ pathname: `/series/${seriesItem.id}`, state: { element: seriesItem.id }}}
        className={`series-${seriesItem.id}`}
      />
    );
  });

  return (
      <div id='seriesList' ref={seriesRef} className={styles.SeriesList}>
        {!props.filtered && <h1>Popular Series</h1>}
        <InfiniteScroll
          className={styles.ScrollArea}
          scrollableTarget='mainLayout'
          dataLength={props.seriesList.length}
          hasMore={props.currentPage < props.pages}
          next={nextPageHandler}
          scrollThreshold={0.95}
          loader={<Spinner />}
        >
          <ul>
            {seriesList.length === 0 && props.filtered ? <h2>Sorry! No Series Found...</h2> : seriesList}
          </ul>
        </InfiniteScroll>
      </div>
  );
});

const mapStateToProps = state => {
  return {
    seriesList: state.seriesList.seriesList,
    pages: state.seriesList.pages,
    currentPage: state.seriesList.currentPage,
    nextPage: state.seriesList.nextPage,
    filters: state.filters.filters,
    filtered: state.seriesList.filtered,
    redirected: state.seriesList.redirected,
    scrollTop: state.seriesList.scrollTop,
    scrollPos: state.seriesList.scrollPos,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchPopularSeries: (page) => dispatch(action.fetchPopularSeries(page)),
    fetchFilteredSeries: (filters, page) => dispatch(action.applyFilters(filters, page)),
    onRedirect: (redirected) => dispatch(action.redirectToSeriesList(redirected)),
    onNextPage: (page) => dispatch(action.nextPage(page)),
    setScrollPos: (scrollPos) => dispatch(action.setScrollPos(scrollPos))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SeriesList);