import React from 'react';

import useHttpClient from '../../hooks/useHttpError';
import ErrorDialog from '../../components/UI/ErrorDialog/ErrorDialog';

const withHttpError = (WrappedComponent, axios, redirect = null) => {

  return props => {

    const [ error, loading, clearError ] = useHttpClient(axios);

    return (
      <React.Fragment>
        {
          !loading &&
          <ErrorDialog {...props} to={redirect} error={error} open={error !== null} close={clearError}/>
        }
        <WrappedComponent {...props} />
      </React.Fragment>
    );
  }
}

export default withHttpError;