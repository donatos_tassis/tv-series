import React, { useEffect, useState, memo } from 'react';

import Slide from '@material-ui/core/Slide';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import theme from '../../helper/breakpoints';
import useMediaQuery from '@material-ui/core/useMediaQuery';

const HideComponent = memo((props) => {

  const [scrollTarget, setScrollTarget] = useState(undefined);
  const [threshold, setThreshold] = useState(50);
  const trigger = useScrollTrigger({ target: scrollTarget, threshold: threshold });
  const { mainRef, init, resetInit } = props;

  useEffect(() => {
    setScrollTarget(mainRef.current);
  },[mainRef, scrollTarget, setScrollTarget]);

  const triggered = (reverse = false) => {
    if (reverse)
      return !trigger;
    return trigger;
  }

  const smallScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const scrollable = scrollTarget && scrollTarget.scrollHeight > scrollTarget.offsetHeight;

  useEffect(() => {
    if (init) {
      setThreshold(mainRef.current.scrollTop);
      resetInit();
    }
  }, [mainRef, setThreshold, init, resetInit])

  return (
    (!scrollable || !smallScreen ) ? props.children :
      <Slide
        appear={false}
        direction={props.direction}
        in={props.init ? props.init : triggered(props.reverse)}>
        {props.children}
      </Slide>
  );
});

export default HideComponent;