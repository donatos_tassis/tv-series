import React from 'react';

import styles from "../Layout/Layout.module.css";

const MainLayout = (props) => {

  return (
    <main id='mainLayout' ref={props.mainRef} className={styles.MainLayout}>
      {props.children}
    </main>
  );
};

export default MainLayout;