import React, { useEffect, useState, memo } from 'react';
import { connect } from 'react-redux';

import Footer from '../../components/Footer/Footer';
import Header from "../../components/Navigation/Header/Header";
import MainLayout from '../MainLayout/MainLayout';
import Filters from '../../containers/Filters/Filters';

import styles from './Layout.module.css';
import * as action from "../../store/actions";

const Layout = memo(props => {
  const [gridClass, setGridClass] = useState(styles.Content);
  const [initFilters, setInitFilters] = useState(false);

  useEffect(() => {
    if (props.showFilters)
      setGridClass(styles.Content);
    else
      setGridClass(styles.ContentNoFilters);
  }, [props.showFilters]);

  const toggleFiltersHandler = () => {
    props.toggleFilters();
    setInitFilters(true);
  }

  const resetInitFilters = () => {
    setInitFilters(false);
  }

  return (
    <div className={gridClass}>
      <Header mainRef={props.mainRef} toggleFilters={toggleFiltersHandler}/>
      {props.showFilters && <Filters mainRef={props.mainRef} init={initFilters} resetInit={resetInitFilters}/>}
      <MainLayout mainRef={props.mainRef} children={props.children}/>
      <Footer mainRef={props.mainRef}/>
    </div>
  );
});

const mapStateToProps = state => {
  return {
    showFilters: state.global.showFilters,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    toggleFilters: () => dispatch(action.toggleFilters()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Layout);